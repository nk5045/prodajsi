package si.prodaj.androidprodaj;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListViewAdapter extends ArrayAdapter<ListViewDataModel> implements View.OnClickListener {

    private ArrayList<ListViewDataModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        ImageView image;
        TextView txtTitle;
        TextView txtPrice;
    }

    public ListViewAdapter(ArrayList<ListViewDataModel> data, Context context) {
        super(context, R.layout.list_view_layout, data);
        this.dataSet = data;
        this.mContext = context;

    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        ListViewDataModel listViewDataModel = (ListViewDataModel) object;

        switch (v.getId()) {
            case R.id.imageViewImage:
                Toast.makeText(getContext(), listViewDataModel.getTitle(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ListViewDataModel listViewDataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_view_layout, parent, false);
            viewHolder.image = convertView.findViewById(R.id.imageViewImage);
            viewHolder.txtTitle =  convertView.findViewById(R.id.textViewTitle);
            viewHolder.txtPrice = convertView.findViewById(R.id.textViewPrice);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtTitle.setText(listViewDataModel.getTitle());
        viewHolder.txtPrice.setText(listViewDataModel.getPrice());
        new DownloadImageTask(viewHolder.image).execute(listViewDataModel.getImage());
        viewHolder.image.setOnClickListener(this);
        viewHolder.image.setTag(position);
        return convertView;
    }
}

