package si.prodaj.androidprodaj;

public class ListViewDataModel {
    String image;
    String title;
    String price;

    public ListViewDataModel(String  image, String title, String price) {
        this.image=image;
        this.title=title;
        this.price=price;
    }

    public String getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }
}
