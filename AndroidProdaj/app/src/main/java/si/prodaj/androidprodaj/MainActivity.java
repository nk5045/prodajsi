package si.prodaj.androidprodaj;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.Response;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    String TAG = "MainActivity";

    Spinner spinnerCategory;
    EditText editTextQuery;
    Button buttonSearch;
    ListView listViewResults;
    RequestQueue requestQueue;

    private static ListViewAdapter listViewAdapter;
    ArrayList<ListViewDataModel> listViewDataModels;

    String baseUrl = "https://prodajsi.azurewebsites.net/";
    String url;

    int currentCategoryId = 0;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinnerCategory = findViewById(R.id.spinnerCategory);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Categories, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerCategory.setAdapter(adapter);
        spinnerCategory.setOnItemSelectedListener(this);

        editTextQuery = findViewById(R.id.editTextQuery);
        buttonSearch = findViewById(R.id.buttonSearch);
        listViewResults = findViewById(R.id.listViewResults);
        listViewResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Click ListItem Number " + position, Toast.LENGTH_LONG)
                        .show();
            }
        });

        listViewDataModels = new ArrayList<>();
        listViewAdapter= new ListViewAdapter(listViewDataModels,getApplicationContext());
        listViewResults.setAdapter(listViewAdapter);
        listViewResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ListViewDataModel dataModel= listViewDataModels.get(position);

                Toast.makeText(getApplicationContext(), String.valueOf(position), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue = Volley.newRequestQueue(this);

    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        Log.d(TAG, "Value: " + String.valueOf(pos));
        currentCategoryId = pos;

    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    public void OnClickSearch(View v) {
        this.url = this.baseUrl + "/api/ads";

        if(currentCategoryId != 0){
            this.url +="?category=" + currentCategoryId;

        }

        if(!editTextQuery.getText().toString().isEmpty()){
            if(!this.url.contains("?")){
                this.url +="?";
            } else {
                this.url +="&";
            }
            this.url += "title=" + editTextQuery.getText().toString();
        }

        JsonArrayRequest arrReq = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, response.toString());
                listViewDataModels = new ArrayList<>();
                if (response.length() > 0) {
                    // The user does have repos, so let's loop through them all.
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            // For each repo, add a new line to our repo list.
                            JSONObject jsonObj = response.getJSONObject(i);
                            String image = jsonObj.get("DisplayImage").toString();
                            String title = jsonObj.get("Title").toString();
                            String price = jsonObj.get("Price").toString();

                            String imageUrl = baseUrl + image.replace("~/", "");

                            listViewDataModels.add(new ListViewDataModel(imageUrl, title, price));

                        } catch (JSONException e) {
                            // If there is an error then output this to the logs.
                            Log.e(TAG, "Invalid JSON Object.");
                        }
                        listViewAdapter= new ListViewAdapter(listViewDataModels,getApplicationContext());
                        listViewResults.setAdapter(listViewAdapter);
                    }
                    //textViewResults.setText(response.toString());
                } else {
                    Toast.makeText(getApplicationContext(), "No results.", Toast.LENGTH_LONG).show();
                    listViewAdapter= new ListViewAdapter(listViewDataModels,getApplicationContext());
                    listViewResults.setAdapter(listViewAdapter);
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // If there a HTTP error then add a note to our repo list.
                        Toast.makeText(getApplicationContext(), "Error while calling REST API", Toast.LENGTH_LONG).show();
                        Log.e("Volley", error.toString());
                    }
                }
        );
        // Add the request we just defined to our request queue.
        // The request queue will automatically handle the request as soon as it can.
        requestQueue.add(arrReq);
    }
}
