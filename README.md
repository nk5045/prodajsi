Prodaj.si
==========

Simon Šegatin (63160294), Nejc Kovač (63160178)

Opis
Prodaj.si je spletna stran za prodajanje in kupovanje različnih izdelkov med potrošniki samimi (consumer-consumer). Na voljo je kot spletna stran ali kot aplikacija za Android.

Spletna aplikacija
Spletna aplikacija je izdelana v ASP.NET MVC 5, teče pa v oblaku Microsoft Azure. Podatkovna baza. Front-end predstavlja spletna stran za iskanje po oglasih. Na strežniku je na voljo tudi REST API preko katerega lahko dostopamo do podatkov o oglasih.
Uporabniku je na voljo iskanje po kategorijah in naslovu oglasa. Vsak oglas mora imeti naslov, ceno in kontakt, lahko pa ima tudi opis in eno ali več slik. Aplikacija podpira tudi registracijo preko ASP.NET Identity modula.
Uporabnik lahko neposredno ustvari lokalni račun, lahko pa se registrira preko Google ali Facebook računa in izbere samo lokalno uporabniško ime. Registrirani uporabniki lahko objavljajo nove oglase ali izbrišejo svoje stare.


Sitemap:


![Sitemap](/slike/sitemap.png?raw=true)

Android odjemalec
Aplikacija je bila izdelana v Javi s pomočjo Android Studia. Aplikacija pošlje GET zahtevek na REST API, kjer dobi nazaj odgovor z vsemi oglasi, ki ustrezajo kriterijem. Kriterij nastavimo z dodatnima atributoma "category" in "title".
Aplikacija ima samo en Activity, na katerem je drop down meni, kjer izberemo kategorijo. Imamo text box, kjer vpišemo željeni oglas, ki ga iščemo. Ko imamo vse kriterije nastavljene pritisnemo gumb "SEARCH" in se nam spodaj prikažejo vsi izdelki, ki ustrezajokriterijem.


Android App:


![Android App](/slike/androidapp.png?raw=true)