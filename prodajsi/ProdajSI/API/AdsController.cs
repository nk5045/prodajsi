﻿using ProdajSI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProdajSI.API
{
    public class AdsController : ApiController
    {
        ProdajEntities db = new ProdajEntities();
        
        public IEnumerable<AdListViewModel> Get(int? category = null, string title = "")
        {
            var _ads = db.Ads.AsQueryable();

            if (category != null)
                _ads = _ads.Where(a => a.id_category == category);

            if (!String.IsNullOrEmpty(title))
                _ads = _ads.Where(a => a.title.ToLower().Contains(title.ToLower()));

            var ads = new List<AdListViewModel>();
            foreach (var _ad in _ads)
            {
                ads.Add(new AdListViewModel
                {
                    Id = _ad.id_ad,
                    Title = _ad.title,
                    Price = _ad.price,
                    Description = _ad.description,
                    DisplayImage = _ad.Images.Count() > 0 ? String.Format("~/Images/{0}/{1}", _ad.id_ad, _ad.Images.First().url) : "~/Images/placeholder.png"
                });
            }

            return ads;
        }

        // POST: api/Ads
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Ads/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Ads/5
        public void Delete(int id)
        {
        }
    }
}
