﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProdajSI.Models;
using Microsoft.AspNet.Identity;
using PagedList;
using System.IO;

namespace ProdajSI.Controllers
{
    public class AdsController : Controller
    {
        ProdajEntities db = new ProdajEntities();


        // GET: Ads
        public ActionResult Index()
        {
            ViewBag.Category = "All Categories";

            var categories = new List<CategoryListViewModel>();
            foreach(var category in db.Categories)
            {
                categories.Add(new CategoryListViewModel
                {
                    Id = category.id_category,
                    Category = category.category
                });
            }
            return View(categories);
        }

        // GET: Ads/Search
        public ActionResult Search(int? category, int? page, string title)
        {
            var _ads = db.Ads.AsQueryable();

            ViewBag.Category = "All Categories";
            if (category != null)
            {
                _ads = _ads.Where(a => a.id_category == category);
                ViewBag.Category = db.Categories.Find(category).category;
            }
            
            if (!String.IsNullOrEmpty(title))
                _ads = _ads.Where(a => a.title.ToLower().Contains(title.ToLower()));
            
            var ads = new List<AdListViewModel>();
            foreach (var _ad in _ads)
            {
                ads.Add(new AdListViewModel
                {
                    Id = _ad.id_ad,
                    Title = _ad.title,
                    Price = _ad.price,
                    Description = _ad.description,
                    DisplayImage = _ad.Images.Count() > 0 ? String.Format("~/Images/{0}/{1}", _ad.id_ad, _ad.Images.First().url) : "~/Images/placeholder.png"
                });
            }

            int pageSize = 10;
                        
            /*if (!Int32.TryParse(Request.QueryString["page"], out page))
                page = 1;*/

            if (page > Math.Ceiling((decimal)_ads.Count() / pageSize))
                page = 1;

            var ads_paged = ads.ToPagedList(page ?? 1, pageSize);
            return View(ads_paged);
        }

        // GET: Ads/Details/5
        public ActionResult Details(int id)
        {
            var _ad = db.Ads.Find(id);
            if (_ad == null)
                Redirect("Error");

            var Images = new List<string>();
            foreach(var img in _ad.Images)
            {
                Images.Add(String.Format("~/Images/{0}/{1}", _ad.id_ad, img.url));
            }

            var ad = new AdDetailsViewModel
            {
                Title = _ad.title,
                PostDate = _ad.post_date,
                Contact = _ad.contact,
                Price = _ad.price,
                Description = _ad.description,
                Category = _ad.Categories.category,
                Region = _ad.Regions.region,
                Images = Images
            };
            return View(ad);
        }

        // GET: Ads/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.Category = new SelectList(db.Categories, "id_category", "category");
            ViewBag.Region = new SelectList(db.Regions, "id_region", "region");

            return View();
        }

        // POST: Ads/Create
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateAdViewModel ad)
        {
            ViewBag.Category = new SelectList(db.Categories, "id_category", "category");
            ViewBag.Region = new SelectList(db.Regions, "id_region", "region");

            if (!ModelState.IsValid)
            {
                return View(ad);
            }

            var _ad = new Ads
            {
                title = ad.Title,
                post_date = DateTime.Now,
                contact = ad.Contact,
                price = ad.Price,
                description = ad.Description,
                id_user = User.Identity.GetUserId(),
                id_category = ad.IdCategory,
                id_region = ad.IdRegion
            };

            foreach (var image in ad.Images)
            {
                if(image != null)
                {
                    if (String.IsNullOrEmpty(Path.GetExtension(image.FileName)) || (
                        Path.GetExtension(image.FileName) != ".jpg" &&
                        Path.GetExtension(image.FileName) != ".jpeg" &&
                        Path.GetExtension(image.FileName) != ".png"
                        ))
                        return View(ad);
                    var filename = Path.GetFileName(image.FileName);
                    _ad.Images.Add(new Images
                    {
                        url = filename
                    });
                }
            }

            db.Ads.Add(_ad);
            db.SaveChanges();

            
            foreach (var image in ad.Images)
            {
                if(image != null)
                {
                    var filename = Path.GetFileName(image.FileName);
                    if (!Directory.Exists(Server.MapPath("~/Images/" + _ad.id_ad)))
                        Directory.CreateDirectory(Server.MapPath("~/Images/" + _ad.id_ad));
                    var savepath = Path.Combine(Server.MapPath("~/Images/" + _ad.id_ad + "/") + filename);
                    image.SaveAs(savepath);
                }
            }

            return RedirectToAction("Index");
        }

        // GET: Ads/userID
        [Authorize]
        public ActionResult AdsByUser(string user = "")
        {
            var _ads = db.Ads.Where(a => a.id_user.Equals(user));

            var ads = new List<AdListViewModel>();
            foreach (var _ad in _ads)
            {
                ads.Add(new AdListViewModel
                {
                    Id = _ad.id_ad,
                    Title = _ad.title,
                    Price = _ad.price,
                    Description = _ad.description,
                    DisplayImage = _ad.Images.Count() > 0 ? String.Format("~/Images/{0}/{1}", _ad.id_ad, _ad.Images.First().url) : "~/Images/placeholder.png"
                });
            }

            return View(ads);
        }

        // GET: Ads/Edit/5
        [Authorize]
        public ActionResult Edit(int id)
        {
            var _ad = db.Ads.Find(id);

            if(_ad == null)
                Redirect("NotFound");

            if (_ad.id_user != User.Identity.GetUserId())
                RedirectToAction("Index");

            ViewBag.Category = new SelectList(db.Categories, "id_category", "category");
            ViewBag.Region = new SelectList(db.Regions, "id_region", "region");

            var ad = new EditAdViewModel
            {
                Title = _ad.title,
                Contact = _ad.contact,
                Price = _ad.price,
                Description = _ad.description,
                IdCategory = _ad.id_category,
                IdRegion = _ad.id_region
            };

            return View(ad);
        }

        // POST: Ads/Edit/5
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, EditAdViewModel ad)
        {
            if (!ModelState.IsValid)
            {
                return View(ad);
            }

            var _ad = db.Ads.Find(id);

            if (_ad.id_user != User.Identity.GetUserId())
                RedirectToAction("Index");
            
            ViewBag.Category = new SelectList(db.Categories, "id_category", "category");
            ViewBag.Region = new SelectList(db.Regions, "id_region", "region");

            _ad.title = ad.Title;
            _ad.contact = ad.Contact;
            _ad.price = ad.Price;
            _ad.description = ad.Description;
            _ad.id_category = ad.IdCategory;
            _ad.id_region = ad.IdRegion;
            db.SaveChanges();

            return RedirectToAction("Details", new { id });
        }

        // POST: Ads/Delete/5
        [Authorize]
        [NonAction]
        public ActionResult Delete(int id)
        {
            var _ad = db.Ads.Find(id);

            if (_ad != null)
                db.Ads.Remove(_ad);

            return RedirectToAction("Index");
        }
    }
}
