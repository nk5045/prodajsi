﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdajSI.Controllers
{
    public class ErrorsController : Controller
    {
        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Notfound()
        {
            return View();
        }
    }
}