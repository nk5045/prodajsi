﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProdajSI.Models
{
    public class CategoryListViewModel
    {
        [Required]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Category")]
        public string Category { get; set; }
    }

    public class AdDetailsViewModel
    {
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Post Date")]
        public DateTime PostDate { get; set; }

        [Required]
        [Display(Name = "Contact")]
        public string Contact { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Price")]
        public double Price { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Category")]
        public string Category { get; set; }

        [Required]
        [Display(Name = "Region")]
        public string Region { get; set; }

        [Required]
        [Display(Name = "Images")]
        public List<string> Images { get; set; }
    }

    public class AdListViewModel
    {
        [Required]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Price")]
        public double Price { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Image")]
        public string DisplayImage { get; set; }
    }

    public class CreateAdViewModel
    {
        public CreateAdViewModel()
        {
            Images = new List<HttpPostedFileBase>();
        }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Contact")]
        public string Contact { get; set; }

        [Required]
        [Range(0.01, Double.MaxValue)]
        [DataType(DataType.Currency)]
        [Display(Name = "Price")]
        public double Price { get; set; }

        [StringLength(500)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Category")]
        public int IdCategory { get; set; }

        [Required]
        [Display(Name = "Region")]
        public int IdRegion { get; set; }

        [Display(Name = "Images")]
        public List<HttpPostedFileBase> Images { get; set; }
    }

    public class EditAdViewModel
    {
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Contact")]
        public string Contact { get; set; }

        [Required]
        [Range(0.01, Double.MaxValue)]
        [DataType(DataType.Currency)]
        [Display(Name = "Price")]
        public double Price { get; set; }

        [StringLength(500)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Category")]
        public int IdCategory { get; set; }

        [Required]
        [Display(Name = "Region")]
        public int IdRegion { get; set; }
    }
}