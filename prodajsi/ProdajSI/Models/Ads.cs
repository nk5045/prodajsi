//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProdajSI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ads
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Ads()
        {
            this.Images = new HashSet<Images>();
        }
    
        public int id_ad { get; set; }
        public string title { get; set; }
        public System.DateTime post_date { get; set; }
        public string contact { get; set; }
        public double price { get; set; }
        public string description { get; set; }
        public string id_user { get; set; }
        public int id_category { get; set; }
        public int id_region { get; set; }
    
        public virtual AspNetUsers AspNetUsers { get; set; }
        public virtual Categories Categories { get; set; }
        public virtual Regions Regions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Images> Images { get; set; }
    }
}
