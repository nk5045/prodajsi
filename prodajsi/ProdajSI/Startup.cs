﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProdajSI.Startup))]
namespace ProdajSI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
